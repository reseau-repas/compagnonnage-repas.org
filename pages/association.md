---
title: Association
header:
  image: /img/iconographie/asso_portage.jpg
---

# L'association
## Un peu d'histoire...
En 1997, le réseau REPAS organisait son premier compagnonnage. Depuis, il n’a cessé d’évoluer, avec notamment en 2016 l’amorce de la refonte pédagogique du parcours qui fût effective en 2018, sans pour autant avoir revu le modèle économique qui l’accompagne. En parallèle, la transmission du portage administratif s’enclenche, entre la structure historiquement porteuse (le Mat) et le collectif organisant le parcours (le comité de pilotage), avec la création de l’association du «Compagnonnage REPAS».
À partir de 2018, le comité de pilotage décide de ne plus se rémunérer, afin de constituer une trésorerie qui permettrait d’assurer une certaine pérennité et souplesse au sein de cette nouvelle structure. Ce choix a en partie porté ses fruits, mais a aussi fragilisé l’équilibre de l’engagement en temps des copilotes entre le compagnonnage et leur propre structure.

## Le compagnonnage aujourd'hui
L’économie du compagnonnage fonctionnait en grande partie avec des subventions publiques, or le contexte politique actuel est à la restriction de ces subventions. Le comité de pilotage se questionne de plus en plus quant à son envie de dépendre de ces aides et d’y mettre de l’énergie. En outre, pour garder la prise en charge du parcours par pôle emploi, l’association devrait rentrer dans des démarches de certifications de plus en plus contraignantes.

Compte tenu de cette situation complexe, le comité de pilotage se réunit depuis fin 2020 pour explorer les différentes pistes qui se présentent pour la suite. Il en découle la décision unanime que 2021 est un moment opportun pour ne pas accueillir de compagnon-ne-s, afin de libérer du temps de réflexion et d’ouvrir le champ des possibles quant à la suite du parcours qui reprendra à l'automne 2022.

## Envie de nous soutenir ?

Vos dons serviront :
- À financer la suite de nos actions et réflexions (trajets, repas, hébergement lors des rencontres …).
- À constituer un fond de solidarité afin de parrainer des compagnon.ne.s n’ayant pas les moyens de financer la totalité du parcours

<a href="https://www.helloasso.com/associations/compagnonnage-repas/formulaires/1/widget" target="_blank" class="cta">Soutenez-nous sur HelloAsso !</a>
