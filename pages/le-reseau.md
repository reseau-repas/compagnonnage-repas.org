---
title: Le réseau R.E.P.A.S.
description: Le Réseau d’Échanges de Pratiques Alternatives et Solidaires
header:
  image: /img/iconographie/reseau.jpg
---

# R.E.P.A.S., C’est quoi ?
Le **R**éseau d’**E**changes de **P**ratiques **A**lternatives et **S**olidaires
est composé d’entreprises coopératives, associations et lieux de vie engagés de façon permanente dans la concrétisation d’un projet de société. Ils produisent des biens et des services afin de satisfaire des besoins de subsistance et d’émancipation. L’argent est seulement considéré comme l’un des outils nécessaires à leur action. Il s’agit de favoriser une économie qui agit directement sur leur environnement de proximité.

# Les acteur.ice.s de l’écosystème du R.E.P.A.S.
- **OÙ ?** Dans des déserts ruraux, des petites villes ou de grosses métropoles.
- **QUI ?** Des équipes composées de deux à plusieurs dizaines de personnes.
- **QUOI ?** Des activités dans l’agriculture, l’artisanat, les métiers de bouche, la scierie, la filature, les métiers du bâtiment, l’ingénierie, la recherche, l’enseignement, l’animation, l’éducation populaire, le commerce, les métiers de l’édition, les ressource-ries, les arts et la culture, l’informatique...
- **COMMENT ?** Des statuts de société anonyme (SA), Scop, société anonyme à revenu limité (SARL), GAEC, SCEA, GIE, Association loi 1901...
- **COMBIEN ?** Des chiffres d’affaire annuels allant de quelques dizaines de milliers d’euros à plusieurs centaines de milliers d’euros

# Un peu d’histoire
En 1994, quelques entreprises et associations qui se connaissaient ou s’étaient rencontrées dans des réseaux alternatifs décident de se retrouver deux fois par an.
L’idée est de fonctionner en réseau informel, sans structure, sans permanent, avec une communication directe entre les acteurs : une rencontre au printemps, une autre à l’automne, avec un lieu et un thème de réflexion définis à la fin de la rencontre précédente.
Le REPAS est né.
Après l’évocation d’un possible compagnonnage alternatif en mai 1995, 23 membres du réseau consacrent un week-end à réfléchir à la future formation en Octobre. 2 ans plus tard, en 1997, la première promotion regroupe 8 Compagnons, 20 ans après, en 2017, ils sont 25.
Depuis 1997, 310 Compagnon.ne.s âgés de 18 à 40 ans se sont formés sur 23 Sessions de compagnonnages et ont été accueillis par plus d’une trentaine de structure (33 en 2017)

# Les éditions R.E.P.A.S.
Les rencontres du réseau, organisées régulièrement depuis 1994, voient également naître les Editions REPAS. Elles publient des récits et témoignages d'expériences alternatives, collectives et solidaires. Ces livres nous montrent qu'il y a toujours place ici et maintenant, comme hier et ailleurs, pour des réalisations qui inscrivent leur sens dans le concret de pratiques libres et solidaires. Elles souhaitent encourager ainsi celles et ceux qui sont insatisfaits du monde dans lequel ils vivent à faire le pas vers d'autres possibles.  

<a href="http://editionsrepas.free.fr/" class="cta"> Vers les éditions R.E.P.A.S</a>


# Le R.E.P.A.S., mais pourquoi ?
## «R» pour Réseau
Un réseau est une organisation qui existe réellement sans être pour autant structurée sous une forme fixe et définitive. L’idée de réseau suggère la mouvance, la flexibilité, la souplesse. Un réseau grossit ou se rétrécit, il enfle et se dégonfle, se structure et se réorganise.
Le réseau tisse des liens et permet les échanges de manière horizontale et évolutive. Il se modifie en fonction des projets et des rencontres.


## «E» pour Echanges
Le E de REPAS témoigne d’une pratique fondatrice du réseau, celle de l’échange. Et ce n’est pas un hasard si le E a ce sens et non pas celui d’«Entreprises» ou d’«Économie», termes qui précèdent si souvent, presque inévitablement, les A et S d’alternative et solidaire... Pourquoi l’absence de ce mot que tant d’autres ont besoin d’afficher dans leurs initiales ? Parce que mettre en avant l’économie, c’est gommer bien des aspects de nos expériences qui, même économiques, dépassent largement ce cadre étroit, devenu hégémonique dans nos sociétés.

## «P» pour Pratiques
C’est peut-être le mot le plus important. La légitimité des entreprises du réseau, de leur témoignage et de leur existence est dans la mise en route réelle et concrète de  pratiques alternatives et solidaires. Le REPAS c’est d’abord un lieu où convergent des pratiques vécues au quotidien depuis de nombreuses années, un étonnant vivier d’initiatives, de réalisations et de constructions dont la seule existence est déjà une sacrée démonstration.

## «A» et «S» pour Alternatives et Solidaires
Alternatives car nos pratiques chantent un petit air qui se démarque du grand orchestre de la mondialisation-marchandisation-économicisation du monde…Solidaires car elles privilégient l’association des individus, la mutualisation des projets et des biens et la coopération entre les hommes plutôt que l’individualisme, la privatisation et la compétition.
