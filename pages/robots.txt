Sitemap: https://compagnonnage-repas.org/sitemap.xml

User-agent: *
Disallow: /site/
Disallow: /site/wp-admin/
Disallow: /partage/
Disallow: /*?page_id
