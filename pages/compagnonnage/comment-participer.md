---
title: Le parcours
order: 6
header:
  icone: backpack
  image: /img/iconographie/participer.jpg
  subtitle: 'Comment participer ?'
---
## Comment participer au compagnonnage R.E.P.A.S. ?

Télécharge et complète le document suivant :

<a href="{{ '/pdf/dossier_de_candidature.pdf' | url }}"  target="_blank" class="cta">Dossier de candidature</a>


Tu peux nous les envoyer par mail à l’adresse : [contact@compagnonnage-repas.org](mailto:contact@compagnonnage-repas.org)<br>
Ou par courrier à : Village de Lauconie 19 150 CORNIL

Date limite de réception de votre dossier :

## 29 septembre 2024

Pour aider à financer ton parcours et plus largement au compagnonnage, tu peux aller chercher des sous auprès de :
mission locale, pôle emploi, service civique,etc.

Le document suivant te donnera quelques éléments de langage pour t'aider dans la démarche :

 <a href="{{ '/pdf/2022_01_06_Document_Ambassadeur.pdf' | url }}" target="_blank" class="cta">Document institutions</a>
