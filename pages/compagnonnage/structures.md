---
title: Le parcours
order: 5
header:
  icone: pin
  image: /img/iconographie/structures.jpg
  subtitle: Structures Accueillantes
mainClass: map
---

## Carte de France des structures

Voici les endroits de France que le compagnonnage pourrait t'amener à découvrir !

Pour se déplacer sur la carte, clic 'n drag, pour zoomer ou dézoomer, utilise la molette de la souris.
<br>

<!-- la carte est insérée à la suite de ce contenu -->
<!-- elle est contruite à partir du fichier `../structures.csv` -->

<div id="structures-carte" 
  data-base-layer-url="{{ '/js/metropole-version-simplifiee.geojson' | url }}"
  data-structures-url="{{ '/api/structures.json' | url }}"
  data-marker-url="{{ '/img/marker.png' | url }}">
</div>


<br>
Pour en savoir plus sur les structures qui accueillent le compagnonnage cette année : <a href="{{ '/pdf/202106_Fiches_Structure.pdf' | url }}" target="_blank" class="cta">En savoir plus</a>

