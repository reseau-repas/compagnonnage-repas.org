---
title: Le parcours
order: 2
header:
  icone: pedagogie
  image: /img/iconographie/pedagogie.jpg
  subtitle: La pédagogie
---
## Un apprentissage de la coopération innovant via…

- Un tutorat au sein de chaque entreprise : le/la compagnon.ne est accompagné.e par un coopérateur.ice attitré.e.
- Un partage in vivo de l’activité professionnelle d’une équipe.
- Des débats collectifs sur des thèmes qui préoccupent les compagnon.ne.s.
- Un chantier sur le site d'une des entreprises pour apprendre à mener un projet collectif de A à Z.
- Un accompagnement personnalisé avec des conseils sur mesure.

## Qu’est ce qu’on met en jeu pendant un compagnonnage ?
- Partager de manière intense différentes expériences de gestion collective
- Questionner les enjeux politiques et sociaux
- Interroger son rapport aux autres
- Eprouver concrètement les utopies qui nous habitent
- Prendre confiance et envisager son avenir
- Avancer en faisant malgré les freins

## Un contenu alliant savoir-faire et réflexion

Les compagnon.ne.s apprennent :

- À connaître les bases de l’économie : maîtriser un budget, développer une vision économique, remettre l’argent au service de l’humain ...
- À faire: revaloriser le sens pratique, le sens des réalités, la polyvalence ...
- À faire-ensemble : prendre des décisions collectivement, mener des réunions plus horizontales, s'exprimer et écouter ...
- À construire un projet professionnel qui a du sens : penser l'utilité sociale d'une activité, s'impliquer sur un territoire,
