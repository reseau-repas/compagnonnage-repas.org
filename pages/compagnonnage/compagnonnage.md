---
title: Le parcours
order: 1
header:
  icone: qui_quoi
  image: /img/iconographie/rando.jpg
  subtitle: 'Quoi ? Pour qui ? Pourquoi ?'
---
## Qu’est ce que le compagnonnage R.E.P.A.S. ?

Le parcours du compagnonnage permet d’interroger des manières de vivre et de travailler. Dans un parcours ponctué de temps de regroupements et d’immersions au sein des structures du réseau, c’est un cheminement dans l’expérimentation et la confrontation de ses propres représentations, principes et valeurs.

## À qui s’adresse ce compagnonnage?

Vous voulez comprendre et expérimenter les enjeux et conditions d’existence des organisations collectives du Réseau.
- Vous  nourrissez questionnements critiques autour du travail et des rapports sociaux qu’il génère ;
- Vous avez envie de participer activement à penser et mettre en œuvre des actions concrètes pour installer une société pérenne, basée sur les valeurs de liberté, d’égalité et de fraternité, et ce malgré les freins financiers, moraux, politiques, culturels ou mythologiques ;
- Vous supposez que pour qu’une telle action soit menée au bout, une organisation collective est nécessaire, basée sur le respect de tous ses individus et sur la valorisation de l’intelligence collective au dépend de l’idolâtrie du chef ;
- Vous avez l’intuition que l’on va plus loin à plusieurs, que l’on grandit au contact des autres, que l’on gagne à coopérer plutôt qu’à se battre ;
- Vous songez ou portez un projet, collectif ou non, mais ancré dans la coopération.

Vous partirez pendant 8 mois en France, au contact des lieux et organisations participantes, vous permettant de confronter des expériences, d’échanger des points de vues et acquérir des compétences nécessaires à la construction de votre avenir individuel et/ou collectif.



## Pourquoi une formation alternative ?

Les collectifs en auto-gestion, qu’ils soient des associations, des entreprises ou des lieux de vie, expérimentent une forme d’innovation sociale et économique. Leurs activités sont socialement utiles, respectueuses de l’environnement, ancrées sur le territoire et créatives dans leur approche de l’économie.
Ces collectifs revitalisent le développement en milieu rural, inventent de nouvelles relations entre producteurs et consommateurs, stimulent l’audace et l’initiative des jeunes. Ils répondent à la nécessité de repenser nos manières de consommer et nos façons de produire.
