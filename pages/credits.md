---
title: Conception et crédits
---

## Design et conception

La splendide équipe du Compagnonnage :

- Elisa Radelet
- Yann Andrieu

## Développement et méthodologie projet

- **Temps total** : {{ temps.total | inHours }} heures ({{ temps.total | inDays }} jours)
- Temps passé par **Sandra** : {{ temps.sandra | inHours }} heures ({{ temps.sandra | inDays }} jours)
- Temps passé par **Thomas** : {{ temps.thomas | inHours }} heures ({{ temps.thomas | inDays }} jours)

Le [code source de ce site est en libre accès](https://framagit.org/reseau-repas/compagnonnage-repas.org).

## Logiciels libres

Merci aux personnes qui ont conçu ces briques logicielles sur leur temps libre,
ou dans le cadre de leur activité professionnelle.

- [Eleventy](https://www.11ty.dev)
- [csv-parse](https://www.npmjs.com/package/csv-parse)
- [MapLibre GL](https://maplibre.org/)
- [munge](https://www.npmjs.com/package/munge)

## Services

- [FramaGit](https://framagit.org)
- [GitLab](https://gitlab.com/gitlab-org/gitlab)

## Données ouvertes

- [France GeoJSON](https://github.com/gregoiredavid/france-geojson)
