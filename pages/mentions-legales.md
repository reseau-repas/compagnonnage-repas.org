---
title: Mentions légales
---

#### Coordonnées de l'association :  ####  
<img src="../img/logo-compagnonnage-repas.svg" alt="logo compagnonnage" width="125" align="left"/>&nbsp;&nbsp;&nbsp;&nbsp;**Association Compagnonnage R.E.P.A.S.**  
&nbsp;&nbsp;&nbsp;&nbsp;Lauconie, 19150 Cornil  
&nbsp;&nbsp;&nbsp;&nbsp;contact@compagnonnage-repas.org  



---

#### Coordonnée de l'hébergeur du site :  ####
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Cliss XXI**  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;23 avenue Jean Jaurès, 62800 Liévin  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;contact@cliss21.com  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;03 21 45 78 24

---

L’association Compagnonnage R.E.P.A.S. s’engage à respecter et à appliquer la réglementation en vigueur en France (Loi Informatique et Libertés du 6 janvier 1978 modifiée) et dans l’Union Européenne (Règlement UE 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l’égard du traitement des données à caractère personnel et à la libre circulation de ces données , dit « RGPD ») en matière de protection des données à caractère personnel. Chaque formulaire  limite la collecte des données personnelles au strict nécessaire afin de respecter la minimisation des données.

Les données personnelles recueillies sont traitées selon des protocoles sécurisés et permettent à l’association de gérer les demandes reçues dans ses applications informatiques (Email). Ces données sont exclusivement utilisées pour les besoins du compagnonnage R.E.P.A.S. dans le cadre des inscriptions en formation et ne sont en aucun cas divulguées à un tiers sauf si autorisation express du propriétaire des données.

Durée de conservation des données  :
* pour les demandes d’information via formulaire de contact ou suite à rendez-vous), 3 ans après la clôture de la demande.
* pour la gestion administrative relative au parcours de formation, 5 ans après la clôture des dossiers.
    
Pour toute information ou exercice de vos droits Informatique et Libertés sur les traitements de données personnelles gérés par le compagnonnage R.E.P.A.S., vous pouvez contacter l'association par mail ou par courrier.


