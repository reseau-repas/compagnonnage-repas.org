# Compagnonnage R.E.P.A.S

Le site de la formation en compagnonnage du Réseau REPAS.

La mise en ligne est automatique, et prend moins d'_1 minute_.

**🧰 Les outils** :

- 🎨 [Les maquettes et architecture d'information][maquettes]
- 📊 [Les actions de travail priorisées](https://www.figma.com/file/bhEMbttpkNcuoT5lm6M1pb/Brainstorming-_-Compagnonnage-REPAS)
- 👁 **Accéder au site**: [compagnonnage-repas.org/](https://compagnonnage-repas.org/)
- 📍 (ici) [_Le code source du site_](https://framagit.org/reseau-repas/compagnonnage-repas.org)

**📚 Les guides** :

- [Mettre à jour les contenus](#mettre-à-jour-les-contenus)
- [Mettre à jour la liste des structures](#mettre-à-jour-la-liste-des-structures)
- [Installer le site sur son ordinateur](#installer-le-site-sur-son-ordinateur)

## Mettre à jour les contenus

Les contenus sont des fichiers stockés dans le répertoire [`pages`](./pages).
Ils suivent l'arborescence décrite sur [Figma][maquettes].

Ils sont écrits avec la **syntaxe Markdown** ([comment écrire avec Markdown ?](https://docs.framasoft.org/fr/grav/markdown.html)).

## Mettre à jour la liste des structures

1. Se rendre sur le fichier [`structures.csv`](structures.csv)
2. Cliquer sur l'icône télécharger ![Download](./img/docs/download-icon.png)
3. Modifier le fichier CSV dans un tableur, type OpenOffice¹
4. Se rendre sur [adresse.data.gouv.fr/csv](https://adresse.data.gouv.fr/csv)
5. Déposer le fichier `structures.csv`
6. Choisir `Adresse` dans la section "Choisir les colonnes à utiliser pour construire les adresses"
7. Cliquer sur le bouton "Lancer le géocodage"
8. Télécharger le résultat (`structures.geocoded.csv`)
9. Cliquer sur le bouton [GitLab "Replace"](structures.csv)
10. Sélectionner `structures.geocoded.csv` puis confirmer avec le bouton "Replace file"

¹: Se préoccuper seulement des colonnes hors `latitude`, `longitude` et `result_*`

La nouvelle carte sera mise en ligne dans 1 minute.

## Installer le site sur son ordinateur

### Installation

```sh
npm install
```

## Voir le résultat sur son ordinateur

```bash
npm start
```

[maquettes]: https://www.figma.com/file/ob67Ub9r2skbTHvSnzGLlS/Prototype-du-site
