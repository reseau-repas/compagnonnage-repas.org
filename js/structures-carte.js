function mapSetup (mapContainer) {
  const { baseLayerUrl, structuresUrl, markerUrl } = mapContainer.dataset
  const { LngLatBounds } = maplibregl

  // Initialise la carte MapLibre avec des options
  const map = new maplibregl.Map({
    // element HTML dans lequel la carte sera construite
    container: mapContainer,
    // niveau de zoom initial (1 = terre entière, 22 = ras du sol)
    zoom: 3,
    // niveau de zoom jusqu'où on peut reculer
    minZoom: 0,
    // niveau de zoom jusqu'où on peut plonger
    maxZoom: 7,
    // coordonnées GPS du centre de la carte (plus ou moins le milieu de la France métropolitaine)
    center: [2.209667, 46.232193],
    // carré de coordonnées GPS dans lequel on peut se déplacer
    // si on ne les précise pas, on peut aller se balader dans d'autres pays
    maxBounds: new LngLatBounds(
      [-17.391908446253353, 41.138099752838855],
      [24.376254930457293, 51.14611670016029],
    ),
    // quand on déplace la carte, actualise le "hash" avec les coordonnées du centre de la carte
    // exemple : #/-2,46,3.5
    hash: true,
    // langue des éléments textuels de la carte, s'il y en a
    locale: 'fr'
  })

  const style  ={
    id: "compagnonnage-repas",
    version: 8,
    sources: {
      // ajoute le découpage de la France métropolitaine comme source de données
      // c'est le fichier `js/metropole-version-simplifiee….geojson
      france: {
        type: "geojson",
        data: baseLayerUrl
      },
      // ajoute l'emplacement des structures REPAS comme source de données
      // c'est le fichier `api/structures.json` construit par `_data/structures.js` et publiées par `pages/api/structures.html`
      structures: {
        type: "geojson",
        data: structuresUrl
      }
    },
    layers: [
      {
        // crée un calque d'affichage basé sur le découpage de la France
        // on remplit la forme vectorielle avec un couleur
        id: 'country-boundaries',
        type: 'fill',
        source: 'france',
        paint: {
          'fill-color': '#E5E5E5',
        }
      },
      {
        // crée un calque d'affichage des structures sous forme de marqueurs
        id: 'structures-points',
        type: 'symbol',
        source: 'structures',
        layout: {
          'icon-image': 'marker',
          'icon-size': 0.75,
          'icon-allow-overlap': true,
        }
      }
    ]
  }

  map.loadImage(markerUrl, function (error, image) {
    if (error) {
      return console.error(error)
    }

    map.addImage('marker', image);
    map.setStyle(style)
  })

  // création d'un objet "popup"
  // on fait des choses avec ce popup lors d'interactions sur la carte
  // @see https://maplibre.org/maplibre-gl-js-docs/api/markers/#popup
  const popup = new maplibregl.Popup({
    closeButton: true,
    closeOnClick: true,
    offset: {
      'top': [0, 0],
      'bottom': [0, 0]
    }
  })

  // quand on clique sur un point du calque `structures-points`, on affiche la popup
  map.on('click', 'structures-points', ({ features }) => {
    const [point] = features
    const { coordinates } = point.geometry
    const { Nom, Adresse, SiteWeb } = point.properties

    const html = `
      <h3>${Nom}</h3>
      <p>${Adresse}</p>
      ${SiteWeb && `<p>
        <a href="${SiteWeb}" target="_blank" rel="noopener noreferrer">${SiteWeb}</a>
      </p>`}
    `

    popup
      .setLngLat(coordinates)
      .setHTML(html)
      .setMaxWidth('300px')
      .addTo(map)
  })

  // on change le curseur de la souris lorsqu'on survole un marquer de structure
  map.on('mouseenter', 'structures-points', function () {
    map.getCanvas().style.cursor = 'pointer'
  })
  map.on('mouseleave', 'structures-points', function () {
    map.getCanvas().style.cursor = ''
  })
}


/**
 * Setup the map on page load, if we can see a `<div id="structures-carte">` element
 */
document.addEventListener('DOMContentLoaded', () => {
  const mapContainer = document.querySelector('#structures-carte')

  if (mapContainer) {
    mapSetup(mapContainer)
  }
})
