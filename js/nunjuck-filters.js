const munge = require('./munge.js')

function inHours (minutes) {
  return Math.round(minutes / 60)
}

function inDays (minutes) {
  return Math.round(minutes / 60 / 6)
}

function antiSpam (email) {
  return munge(email, { encoding: 'utf8' })
}

module.exports = { inHours, inDays, antiSpam }
