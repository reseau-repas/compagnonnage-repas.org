const assert = require('assert')
const { describe, test } = require('mocha')
const { inDays, inHours } = require('./nunjuck-filters.js')

describe('inHours', () => {
  test('minutes are rounded as hours', () => {
    assert.equal(inHours(140), 2) // 2.3 hours are rounded down to 2
    assert.equal(inHours(150), 3) // 2.5 hours are rounded up to 3
  })
})

describe('inDays', () => {
  test('minutes are rounded as days', () => {
    assert.equal(inDays(53 * 60), 9) // 8.89 days/53 hours are rounded up to 9
    assert.equal(inDays(56 * 60), 9) // 9.33 days/56 hours are rounded down to 9
  })
})
